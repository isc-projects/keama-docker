# Dockerfile for the Kea Migration Assistant (KeaMA) image

This Docker image is based on Alpine and includes [KeaMA](https://gitlab.isc.org/isc-projects/keama)
with a basic [web interface](https://gitlab.isc.org/isc-projects/keama/-/blob/master/doc/web.md)
written in Flask/Python.

During the image-build process KeaMA is compiled from the source code (which
provides the latest available version), but Docker multi-stage builds keep the
resulting Docker image small.

## Build the image

Use the following commands to build the image
([Docker](https://docs.docker.com/engine/install/) must be installed):

```shell
git clone https://gitlab.isc.org/isc-projects/keama-docker.git
cd keama-docker

docker build -t keama-web .
```

This will clone the repository and build the container image named `keama-web`.
The '.' at the end of the command instructs Docker to look for the Dockerfile
in the current directory.

## Create and run a new container from an image

**On the local machine**, run the KeaMA web interface container on the localhost
only (not publicly exposed):

```shell
docker run --name keama-web --rm -d -p 127.0.0.1:8080:8080/tcp keama-web
```

This command will create and run a new container named `keama-web` from the
image named `keama-web`.
The web interface will be available on http://127.0.0.1:8080 (on your local
machine/host).

Note:
- `--rm` automatically removes the container when it exits
- `-d` runs the container in the background
- `-p` binds port 8080 of the container to TCP port 8080 on 127.0.0.1 of the
host machine

**On the server (remote machine)**, run this command to make the KeaMA web
interface publicly available (to listen on all public IPs, port 8080):

```shell
docker run --name keama-web --rm -d -p 8080:8080 keama-web
```

## Stop/remove the container

To stop the container, run:
```shell
docker stop keama-web
```

The `--rm` option we used above indicates that the container will
be automatically removed; 
if the `--rm` option was not initially included, the `docker rm keama-web`
command removes the container.

# Docker Compose
You can also run the KeaMA web interface via [Docker Compose](https://docs.docker.com/compose/).
The Docker Compose file `compose.yaml` defines the KeaMA web interface service
with [Gunicorn](https://gunicorn.org/) as WSGI HTTP Server and automatically
creates the [named volume](https://docs.docker.com/storage/volumes/)
"savedconfig" (persistent data storage).

Create and start the container via these commands:
```shell
git clone https://gitlab.isc.org/isc-projects/keama-docker.git
cd keama-docker

docker compose up --build -d
```

Note:
- `--build` builds the image before starting container
- `-d` runs the container in the background

ISC uses the same Compose file to run the "KeaMA web interface service" publicly
available at [https://dhcp.isc.org](https://dhcp.isc.org), and uses the
"savedconfig" named volume to store configuration files **if a user explicitly
selects** the "share it with ISC" option. These files are processed and moved off
of the web server automatically. 

You can leverage such an option to store your configuration files "outside" the
container. You can find the exact mount point of the "named volume" by running:
```shell
docker volume inspect keama-docker_savedconfig | grep Mountpoint
```

To stop the container and remove it:
```shell
docker compose down
```

To stop the container and remove it with the docker image and the named volume:
```shell
docker compose down --rmi all -v
```

Note:
- `--rmi` removes the image used by the service
- `-v` removes the named volume declared in the Compose file

## Documentation

- [KeaMA documentation](https://gitlab.isc.org/isc-projects/keama/-/tree/master/doc)
- [Kea documentation](https://kea.readthedocs.io)
- Further documentation, including many short FAQs, is available in
[ISC's Knowledgebase](kb.isc.org/)

## License

KeaMA and Kea are licensed under the [MPL2.0 license](https://www.isc.org/licenses/).
