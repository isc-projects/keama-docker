# SPDX-License-Identifier: MPL-2.0

# first stage
FROM python:alpine AS builder

# set working directory
WORKDIR /

# clone and compile keama
ARG BRANCH=master
RUN apk add git gcc musl-dev make file linux-headers perl
RUN git clone -b ${BRANCH} --single-branch --depth 1 https://gitlab.isc.org/isc-projects/keama.git && \
    cd keama    && \
    ./configure && \
    make

# second stage
FROM python:alpine

# IMAGE_VERSION - reflects the version of the keama tool and can be set from the command line using --build-arg
# docker build --build-arg IMAGE_VERSION=<value> -t keama-web .
ARG IMAGE_VERSION="4.4.3-git"

# https://github.com/opencontainers/image-spec/blob/main/annotations.md#pre-defined-annotation-keys
LABEL org.opencontainers.image.authors="Kea Developers <kea-dev@lists.isc.org>"
LABEL org.opencontainers.image.title="KeaMA web interface"
LABEL org.opencontainers.image.description="KeaMA web interface"
LABEL org.opencontainers.image.version=${IMAGE_VERSION}
LABEL org.opencontainers.image.url="https://gitlab.isc.org/isc-projects/keama-docker"
LABEL org.opencontainers.image.documentation="https://gitlab.isc.org/isc-projects/keama-docker"
LABEL org.opencontainers.image.source="https://gitlab.isc.org/isc-projects/keama-docker"
LABEL org.opencontainers.image.licenses="MPL-2.0"
LABEL org.opencontainers.image.base.name="docker.io/library/python:alpine"

# upgrade pip
RUN pip install --upgrade pip

# get gunicorn and flask
RUN pip install gunicorn
RUN pip install flask

# get curl for healthchecks
RUN apk add curl

# nonroot user for tightened security
RUN adduser -D nonroot
RUN mkdir /home/app/ && chown -R nonroot:nonroot /home/app
WORKDIR /home/app
USER nonroot

# copy keama binary and KeaMA web app from the first stage
COPY --chown=nonroot:nonroot --from=builder /keama/keama/keama ./
COPY --chown=nonroot:nonroot --from=builder /keama/web/ ./
COPY --chown=nonroot:nonroot --from=builder /keama/ChangeLog.md ./

# set env var with correct path to keama binary
ENV FLASK_KEAMA_PATH=/home/app/keama

# web app requirements
RUN pip install -r requirements.txt

# port number the container should expose
EXPOSE 8080

# flask
#CMD [ "flask", "--no-debug", "--app", "app", "run", "--host", "0.0.0.0", "--port", "8080"]

# gunicorn - WSGI HTTP Server (production usage)
CMD [ "gunicorn", "-w", "4", "-t", "60", "-b", "0.0.0.0:8080", "app:app"]
